<?php


    //var_dump($_POST);

    if(isset($_POST["StudentID"])){

        echo "<h1>Showing Person Information"
        $objPerson = new Person();
        $objPerson->setData($_POST);

        echo $objPerson->getName()."<br>";
        echo $objPerson->getDob()."<br>";
        echo $objPerson->getGender()."<br>";
    }

class Person{

    protected $name;
    protected $dob;
    protected $gender;

    public function setData($post_array){

        if(array_key_exists("name",$post_array)){

            $this->name = $post_array["name"];
        }

        if(array_key_exists("dob",$post_array)){

            $this->dob = $post_array["dob"];
        }

        if(array_key_exists("gender",$post_array)){

            $this->gender = $post_array["gender"];
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDob()
    {
        return $this->dob;
    }

    public function getGender()
    {
        return $this->gender;
    }
}