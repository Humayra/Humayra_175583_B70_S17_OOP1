<?php


    class Person{

        protected $name;
        protected $dob;
        protected $gender;

        /**
         * @param mixed $name
         */
        public function setName($name)
        {
            $this->name = $name;
        }

        /**
         * @return mixed
         */
        public function getName()
        {
            return $this->name;
        }
        /**
         * @param mixed $dob
         */
        public function setDob($dob)
        {
            $this->dob = $dob;
        }

        /**
         * @return mixed
         */
        public function getDob()
        {
            return $this->dob;
        }

        /**
         * @param mixed $gender
         */
        public function setGender($gender)
        {
            $this->gender = $gender;
        }

        /**
         * @return mixed
         */
        public function getGender()
        {
            return $this->gender;
        }
    }

    $objPerson = new Person();
    $objPerson->setName("Karim");
    $objPerson->setDob("01-01-2017");
    $objPerson->setGender("Male");

    echo $objPerson->getName()."<br>";
    echo $objPerson->getDob()."<br>";
    echo $objPerson->getGender()."<br>";






die;
    class Person{

        public $name = "ParentName";
        protected $dob = "ParentDOB";
        public $gender = "ParentGender";
        public function doingSomething(){

            echo "I'm inside ".__METHOD__;
        }
        public function doSomething(){
            $this->doSomething();
            echo $this->gender."<br>";
        }
    }

    //$objPerson = new Person();
    //$objPerson->

    $objPerson1 = new Person();
    $objPerson2 = new Person();
    $objPersonN = new Person();

    $objPerson1->name = "abc";
    $objPerson2->name = "nmk";
    $objPersonN->name = "bnh";

    echo $objPerson1->name;

die;
    class Student extends Person{

            public $StudentID;

            public function doSomething(){

                $this->dob = "ChildDOB";
                echo $this->dob."<br>";

                $objPerson1 = new Person();
                echo $objPerson1->dob."<br>";
            }
        }

    $objStudent = new Student();
    echo $objStudent->doSomething();
